import 'package:cartoons_flutter/model/character.dart';

final characters = [
  Character(
    name: 'Julio Rocklee',
    age: 28,
    image: 'images/albert.png',
    jobTitle: 'Rockero',
    stars: 4.3,
  ),
  Character(
    name: 'Gustavo Zamora',
    age: 21,
    image: 'images/gerard.png',
    jobTitle: 'Programador',
    stars: 3.8,
  ),
  Character(
    name: 'Iter Venado',
    age: 33,
    image: 'images/ignasi.png',
    jobTitle: 'Cajero',
    stars: 4.9,
  ),
  Character(
    name: 'Cinthia Cabra',
    age: 29,
    image: 'images/meritxell.png',
    jobTitle: 'Juez',
    stars: 4.1,
  ),
  Character(
    name: 'Maria Corozo',
    age: 24,
    image: 'images/monica.png',
    jobTitle: 'Abogada',
    stars: 3.5,
  ),
  Character(
    name: 'Jose Perez',
    age: 19,
    image: 'images/pol.png',
    jobTitle: 'Auditor',
    stars: 2.9,
  ),
  Character(
    name: 'Mirca Zabala',
    age: 35,
    image: 'images/raquel.png',
    jobTitle: 'Administradora',
    stars: 3.8,
  ),
  Character(
    name: 'Lourdes Campozano',
    age: 31,
    image: 'images/rebeca.png',
    jobTitle: 'Arquitecta',
    stars: 4.6,
  ),
  Character(
    name: 'Juliza Cepeda',
    age: 22,
    image: 'images/ricard.png',
    jobTitle: 'Directora Juducial',
    stars: 4.0,
  ),
  Character(
    name: 'Jorge Alfaro',
    age: 27,
    image: 'images/silvia.png',
    jobTitle: 'Presidente',
    stars: 3.9,
  ),
];
