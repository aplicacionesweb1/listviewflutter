import 'package:flutter/material.dart';
import 'package:cartoons_flutter/model/character.dart';

class CharacterWidget extends StatelessWidget {
  final Character character;
  final Function onDoubleTap;

  const CharacterWidget({Key? key, required this.character, required this.onDoubleTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Container(
      
      margin: const EdgeInsets.all(10),
      height: 150,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: const Color.fromARGB(255, 56, 207, 101)),
        color: Colors.indigo,
      ),

      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Image.asset(
              character.image,              
              height: 150,
            ),

            Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Text(
                    character.name,
                    textScaleFactor: 2,
                    style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.white)
                  ),
                  
                  Container(
                      child: Row(
                    children: [
                      Container(
                          margin: const EdgeInsets.all(5),
                          height: 40,
                          width: 40,
                          padding: const EdgeInsets.only(left: 5),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.white),
                              color: const Color.fromARGB(255, 96, 239, 104),
                              shape: BoxShape.circle),
                          child: Row(
                            children: [
                              Text(character.stars.toString(),
                                  textScaleFactor: 1.25,
                                  style: const TextStyle( color: Colors.white))
                            ],
                          )),
                      Text(character.jobTitle, 
                      textScaleFactor: 1.25,
                      style: TextStyle( color: Colors.teal.shade50))
                    ],
                  ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

    